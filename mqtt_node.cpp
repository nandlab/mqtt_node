#include "mqtt_node.hpp"
#include <boost/bind/bind.hpp>
#include <boost/asio/post.hpp>
#include <boost/asio/placeholders.hpp>
#include <chrono>
#include <sstream>
#include <iomanip>

using namespace boost::placeholders;

using std::string;
typedef mqtt_node self;

const string self::topic_status ("status");
const string self::topic_signal ("signal");
const string self::status_conn              ("ONLINE");
const string self::status_disconn_clean     ("OFFLINE");
const string self::status_disconn_abnorm    ("FAILED");
const string self::status_disconn_exception ("PANIC");
const string self::signal_kill  ("KILL");
const std::chrono::seconds self::reconnect_delay (10);

self::mqtt_node(
    boost::asio::io_context &ioc,
    std::string node_name,
    std::string broker_uri
)
    : exiting(false),
      ioc(ioc),
      node_name(std::move(node_name)),
      full_topic_signal(full_topic(topic_signal)),
      full_topic_status(full_topic(topic_status)),
      connect_options(
          mqtt::connect_options_builder()
          .automatic_reconnect(false)
          .will(mqtt::message(full_topic_status, status_disconn_abnorm, 0, true))
          .finalize()),
      mqtt_client(broker_uri, node_name),
      reconnect_timer(ioc),
      system_signals(ioc, SIGINT, SIGTERM, SIGHUP)
{
    mqtt_client.set_callback(*this);
}

self::~mqtt_node() {
    exiting = true;
    try {
        mqtt_client.disable_callbacks();
        if (mqtt_client.is_connected()) {
            mqtt_client.publish(full_topic_status, status_disconn_exception, 0, true);
            mqtt_client.disconnect()->wait();
        }
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Exception in self dtor: " << e.what() << "." << std::endl;
    }
}

static std::string time_str() {
    using namespace std;
    chrono::system_clock::time_point currentTime = chrono::system_clock::now();
    const time_t time = chrono::system_clock::to_time_t(currentTime);
    chrono::milliseconds ms =
            chrono::duration_cast<std::chrono::milliseconds>(currentTime.time_since_epoch())
            % chrono::milliseconds(1000);
    if (time == (time_t)(-1)) {
        return "NOTIME0";
    }
    else {
        const tm *tm = gmtime(&time);
        if (!tm) {
            return "NOTIME1";
        }
        else {
            ostringstream oss;
            oss << put_time(tm, "%Y-%m-%d %H:%M:%S") << "." << setfill('0') << setw(3) << ms.count();
            return oss.str();
        }
    }
}

std::string self::debug_prefix() {
    return "[" + time_str() + "] " + node_name + ": ";;
}

void self::connected(const std::string& cause) {
    boost::asio::post(ioc, boost::bind(&self::connected_weak, weak_from_this(),
                                       std::make_shared<const std::string>(cause)));
}

void self::connected_weak(wptr_t weak_this, mqtt::string_ptr cause) {
    ptr_t p = weak_this.lock();
    if (p) { p->connected_ioc(cause); }
}

void self::connected_ioc(mqtt::string_ptr cause) {
    if (!exiting) {
        std::cout << debug_prefix() << "Connected";
        if(cause->size()) {
            std::cout << ", cause: " << *cause;
        }
        std::cout << std::endl;
        connected_action_catch();
    }
}

void self::connected_action() {
    std::cout << debug_prefix() << "Subscribing to topic '" << full_topic_signal << "'" << std::endl;
    mqtt_client.subscribe(full_topic_signal, 0);
    std::cout << debug_prefix() << "Publishing status '" << status_conn << "' on topic '" << full_topic_status << "'" << std::endl;
    mqtt_client.publish(full_topic_status, status_conn, 0, true);
}

void self::connected_action_catch() {
    try {
        connected_action();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "connected_action() threw " << e.what() << std::endl;
    }
}

void self::connection_lost(const std::string& cause) {
    boost::asio::post(ioc, boost::bind(&self::connection_lost_weak, weak_from_this(),
                                       std::make_shared<const std::string>(cause)));
}

void self::connection_lost_weak(wptr_t weak_this, mqtt::string_ptr cause) {
    ptr_t p = weak_this.lock();
    if (p) { p->connection_lost_ioc(cause); }
}

void self::connection_lost_ioc(mqtt::string_ptr cause) {
    if (!exiting) {
        std::cout << debug_prefix() << "Connection lost!";
        if(cause->size()) {
            std::cout << " Cause: " << *cause;
        }
        std::cout << std::endl;
        start_reconnect_timer();
    }
}

void self::message_arrived(mqtt::const_message_ptr msg) {
    boost::asio::post(ioc, boost::bind(&self::message_arrived_weak, weak_from_this(), msg));
}

void self::message_arrived_weak(wptr_t weak_this, mqtt::const_message_ptr msg) {
    ptr_t p = weak_this.lock();
    if (p) { p->message_arrived_ioc(msg); }
}

void self::message_arrived_ioc(mqtt::const_message_ptr msg) {
    if (!exiting) {
        std::cout << debug_prefix() << "Message arrived: " << msg->get_topic() << " " << msg->to_string() << std::endl;
        if (!interpret_message(msg)) {
            std::cout << debug_prefix() << "Message unknown" << std::endl;
        }
    }
}

bool self::interpret_message(mqtt::const_message_ptr msg) {
    if (msg->get_topic() == full_topic_signal && msg->to_string() == signal_kill) {
        std::cout << debug_prefix() << "Message is kill, exiting ..." << std::endl;
        clean_exit();
        return true;
    }
    return false;
}

void self::on_failure(const mqtt::token &token) {
    (void)token;
    boost::asio::post(ioc, boost::bind(&self::on_failure_weak, weak_from_this()));
}

void self::on_failure_weak(wptr_t weak_this) {
    ptr_t p = weak_this.lock();
    if (p) { p->on_failure_ioc(); }
}

void self::on_failure_ioc() {
    if (!exiting) {
        std::cout << debug_prefix() << "Connection attempt failed!" << std::endl;
        start_reconnect_timer();
    }
}

void self::on_success(const mqtt::token &token) {
    (void)token;
    boost::asio::post(ioc, boost::bind(&self::on_success_weak, weak_from_this()));
}

void self::on_success_weak(wptr_t weak_this) {
    ptr_t p = weak_this.lock();
    if (p) { p->on_success_ioc(); }
}

void self::on_success_ioc() {
    if (!exiting) {
        std::cout << debug_prefix() << "Connection success" << std::endl;
    }
}

void self::start_reconnect_timer() {
    std::cout << debug_prefix() << "Reconnecting in " << reconnect_delay.count() << "s ..." << std::endl;
    reconnect_timer.expires_after(reconnect_delay);
    wptr_t weak_this = weak_from_this();
    reconnect_timer.async_wait([weak_this](const boost::system::error_code &ec) {
        ptr_t p = weak_this.lock();
        if (p) { p->reconnect(ec); }
    });
}

void self::reconnect(const boost::system::error_code &ec) {
    if (!ec && !exiting && !mqtt_client.is_connected()) {
        std::cout << debug_prefix() << "Reconnecting now ..." << std::endl;
        mqtt_client.connect(connect_options, nullptr, *this);
    }
}

std::string self::full_topic(const std::string &topic) {
    return node_name + "/" + topic;
}

void self::clean_exit() {
    exiting = true;
    system_signals.cancel();
    reconnect_timer.cancel();
    try {
        std::cout << debug_prefix() << "Publishing status '" << status_disconn_clean
                  << "' on topic '" << full_topic_status << "' ..." << std::endl;
        mqtt_client.publish(full_topic_status, status_disconn_clean, 0, true)->wait();
        std::cout << debug_prefix() << "Offline status published" << std::endl;
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Publishing offline status threw " << e.what() << std::endl;
    }
    try {
        std::cout << debug_prefix() << "Disconnecting from broker ..." << std::endl;
        mqtt_client.disconnect()->wait();
        std::cout << debug_prefix() << "Disconnected" << std::endl;
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "Disconnect threw " << e.what() << std::endl;
    }
    try {
        mqtt_client.disable_callbacks();
    }
    catch (const mqtt::exception &e) {
        std::cout << debug_prefix() << "disable_callbacks() threw " << e.what() << std::endl;
    }
}

void self::system_signal_handler(const boost::system::error_code &ec, int signal) {
    if (!ec && !exiting) {
        std::cout << debug_prefix() << "Signal " << signal << " received" << std::endl;
        clean_exit();
    }
}

void self::start() {
    exiting = false;
    std::cout << debug_prefix() << "Connecting to the mqtt broker ..." << std::endl;
    wptr_t weak_this = weak_from_this();
    system_signals.async_wait([weak_this](const boost::system::error_code &ec, int signal) {
        ptr_t p = weak_this.lock();
        if (p) { p->system_signal_handler(ec, signal); }
    });
    mqtt_client.connect(connect_options, nullptr, *this);
}
